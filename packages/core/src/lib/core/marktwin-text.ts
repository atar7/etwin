import { $Ucs2String } from "kryo/lib/ucs2-string.js";

/**
 * Raw marktwin content.
 */
export type MarktwinText = string;

export const $MarktwinText = $Ucs2String;
