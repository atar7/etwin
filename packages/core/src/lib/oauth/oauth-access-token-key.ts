import { $Ucs2String, Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type OauthAccessTokenKey = string;

export const $OauthAccessTokenKey: Ucs2StringType = $Ucs2String;
