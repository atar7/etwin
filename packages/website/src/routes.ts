/**
 * List of routes corresponding to pages (not assets).
 */
export const ROUTES: readonly string[] = [
  "/",
  "/forum",
  "/forum/sections/:section_id",
  "/forum/sections/:section_id/new",
  "/forum/threads/:thread_id",
  "/forum/threads/:thread_id/reply",
  "/legal",
  "/login",
  "/register",
  "/register/email",
  "/register/username",
  "/users/:user_id",
  "/settings"
];
